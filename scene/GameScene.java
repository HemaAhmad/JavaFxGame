package scene;


import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import multimedia.graphics.Background;

public abstract class GameScene {
    private Background background;

    public abstract void init();

    public abstract void update();

    public abstract void render(GraphicsContext gc);

    public abstract void keyPressed(KeyCode keyCode);

    public abstract void keyReleased(KeyCode keyCode);
}
