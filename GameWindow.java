import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import scene.GameSceneManager;

public class GameWindow extends Canvas {
    private GraphicsContext gc;

    public GameWindow(int width, int height){
        super(width, height);
        gc = getGraphicsContext2D();
    }

    public void update(){
        GameSceneManager.getInstance().getCurrentGameScene().update();
    }

    public void render(){
        gc.clearRect(0, 0, getWidth(), getHeight());
        GameSceneManager.getInstance().getCurrentGameScene().render(gc);
    }
}
