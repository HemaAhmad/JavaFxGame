package multimedia;

import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.media.Media;

import java.io.File;

public class MultimediaHandler {
    public static final String resourcesDir = "src/resources/";

    public static Image getImageByName(String imageName) {
        Image image = null;
        File imageFile = new File(resourcesDir + imageName);

        if(imageFile.isFile()) {
            image = new Image(imageFile.toURI().toString());
        }
        else {
            System.out.println("no file");
        }

        return image;
    }

    public static Media getMusicByName(String musicName) {
        Media music = null;

        File musicFile = new File(resourcesDir + musicName);

        if(musicFile.isFile()) {
            music = new Media(musicFile.toURI().toString());
        }
        else {
            System.out.println("no file");
        }

        return music;
    }

    public static WritableImage getSubImage(Image image, int x, int y, int width, int height) {
        return new WritableImage(image.getPixelReader(), x, y, width, height);
    }
}
