package multimedia.graphics;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import multimedia.MultimediaHandler;


public class Background {
    private Image image;

    public Background(String imageName){
        image = MultimediaHandler.getImageByName(imageName);
    }

    public void render(GraphicsContext gc){
        gc.drawImage(image, 0, 0, 640, 480);
    }
}
